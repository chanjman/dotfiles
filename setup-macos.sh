#!/usr/bin/env bash

main() {
    info "macOS setup started."
    # First things first, asking for sudo credentials
    ask_for_sudo

    # Installing Homebrew, the basis of anything and everything
    install_homebrew

    # Installing mas using brew as the requirement for login_to_app_store
    brew_install mas

    # Ensuring the user is logged in the App Store so that
    # install_packages_with_brewfile can install App Store applications
    # using mas cli application
    login_to_app_store

    # Cloning Dotfiles repository for install_packages_with_brewfile
    # to have access to Brewfile
    clone_dotfiles_repo

    # Installing all packages in Dotfiles repository's Brewfile
    install_packages_with_brewfile

    # Installing yarn packages
    yarn_packages=(browser-sync create-react-app netlify-cli eslint stylelint stylelint-config-standard)
    yarn_install "${yarn_packages[@]}"

    # Setting up symlinks
    setup_symlinks

    # Install rvm and latest ruby version
    install_rvm_and_ruby

    # Configuring iTerm2
    configure_iterm2

    # Update /etc/hosts
    update_hosts_file

    # Setting up macOS defaults
    setup_macOS_defaults

    # Updating login items
    update_login_items

    success "macOS setup is done! Restart your computer for some changes to take effect."
}

DOTFILES_REPO=~/Personal/Code/Dotfiles

function ask_for_sudo() {
    info "Prompting for sudo password..."
    if sudo --validate; then
        # Keep-alive
        while true; do sudo --non-interactive true; \
            sleep 60; kill -0 "$$" || exit; done 2>/dev/null &
        success "Sudo credentials updated."
    else
        error "Obtaining sudo credentials failed."
        exit 1
    fi
}

function login_to_app_store() {
    info "Logging into app store..."
    if mas account >/dev/null; then
        success "Already logged in."
    else
        open -a "/Applications/App Store.app"
        until (mas account > /dev/null);
        do
            sleep 3
        done
        success "Login to app store successful."
    fi
}

function install_homebrew() {
    info "Installing Homebrew..."
    if hash brew 2>/dev/null; then
        success "Homebrew already exists."
    else
        url=https://raw.githubusercontent.com/Homebrew/install/master/install
        if /usr/bin/ruby -e "$(curl -fsSL ${url})"; then
            success "Homebrew installation succeeded."
        else
            error "Homebrew installation failed."
            exit 1
        fi
    fi
}

function install_packages_with_brewfile() {
    info "Installing packages within ${DOTFILES_REPO}/brew/Brewfile ..."
    if brew bundle --file=$DOTFILES_REPO/brew/Brewfile; then
        success "Brewfile installation succeeded."
    else
        error "Brewfile installation failed."
        exit 1
    fi
}

function brew_install() {
    package_to_install="$1"
    info "brew install ${package_to_install}"
    if hash "$package_to_install" 2>/dev/null; then
        success "${package_to_install} already exists."
    else
        if brew install "$package_to_install"; then
            success "Package ${package_to_install} installation succeeded."
        else
            error "Package ${package_to_install} installation failed."
            exit 1
        fi
    fi
}

function clone_dotfiles_repo() {
    info "Cloning dotfiles repository into ${DOTFILES_REPO} ..."
    if test -e $DOTFILES_REPO; then
        substep "${DOTFILES_REPO} already exists."
        pull_latest $DOTFILES_REPO
    else
        url=https://gitlab.com/chanjman/dotfiles.git
        if git clone "$url" $DOTFILES_REPO; then
            success "Cloned into ${DOTFILES_REPO}"
        else
            error "Cloning into ${DOTFILES_REPO} failed."
            exit 1
        fi
    fi
}

function pull_latest() {
    info "Pulling latest changes in ${1} repository..."
    if git -C $1 pull origin master &> /dev/null; then
        success "Pull successful in ${1} repository."
    else
        error "Please pull the latest changes in ${1} repository manually."
    fi
}

function configure_iterm2() {
    info "Configuring iTerm2..."
    if \
        defaults write com.googlecode.iterm2 \
            LoadPrefsFromCustomFolder -int 1 && \
        defaults write com.googlecode.iterm2 \
            PrefsCustomFolder -string "${DOTFILES_REPO}/iTerm2";
    then
        success "iTerm2 configuration succeeded."
    else
        error "iTerm2 configuration failed."
        exit 1
    fi
    substep "Opening iTerm2"
    if osascript -e 'tell application "iTerm" to activate'; then
        substep "iTerm2 activation successful"
    else
        error "Failed to activate iTerm2"
        exit 1
    fi
}

function setup_symlinks() {
    info "Setting up symlinks..."
    symlink_files
    success "Symlinks successfully setup."
}

function symlink_files() {
    for source in `find $DOTFILES_REPO/symlinks -name \*.symlink`
    do
    destination="$HOME/.`basename \"${source%.*}\"`"

    if rm -rf $destination && ln -s $source $destination; then
        success "Linked ${source} to ${destination}"
    else
        error "Symlinking ${source} failed."
        exit 1
    fi
    done
}

function install_rvm_and_ruby() {
    curl -sSL https://get.rvm.io | bash -s stable --rails
}

function update_hosts_file() {
    info "Updating /etc/hosts"

    if grep --quiet "someonewhocares" /etc/hosts; then
        success "/etc/hosts already updated."
    else
        substep "Backing up /etc/hosts to /etc/hosts_old"
        if sudo cp /etc/hosts /etc/hosts_old; then
            substep "Backup succeeded."
        else
            error "Backup failed."
            exit 1
        fi
        substep "Appending ${DOTFILES_REPO}/hosts/hosts content to /etc/hosts"
        if test -e ${DOTFILES_REPO}/hosts/hosts; then
            cat ${DOTFILES_REPO}/hosts/hosts | \
                sudo tee -a /etc/hosts > /dev/null
            success "/etc/hosts updated."
        else
            error "Failed to update /etc/hosts"
            exit 1
        fi
    fi
}

function setup_macOS_defaults() {
    info "Setting macOS defaults..."

    current_dir=$(pwd)
    cd ${DOTFILES_REPO}/macOS
    if bash defaults.sh; then
        cd $current_dir
        success "macOS defaults setup succeeded (some of these changes require a logout/restart to take effect)."
    else
        cd $current_dir
        error "macOS defaults setup failed."
        exit 1
    fi
}

function update_login_items() {
    info "Updating login items..."

    login_item /Applications/Dropbox.app
    login_item /Applications/Transmission.app

    success "Login items successfully updated."
}

function login_item() {
    path=$1
    hidden=${2:-false}
    name=$(basename "$path")

    # "¬" charachter tells osascript that the line continues
    if osascript &> /dev/null << EOM
tell application "System Events" to make login item with properties ¬
{name: "$name", path: "$path", hidden: "$hidden"}
EOM
then
    success "Login item ${name} successfully added."
else
    error "Adding login item ${name} failed."
    exit 1
fi
}

function yarn_install() {
    packages_to_install=("$@")

    for package_to_install in "${packages_to_install[@]}"
    do
        info "yarn global add ${package_to_install}"
        if yarn global list | grep "$package_to_install" &> /dev/null; then
            success "${package_to_install} already exists."
        else
            if yarn global add "$package_to_install"; then
                success "Package ${package_to_install} installation succeeded."
            else
                error "Package ${package_to_install} installation failed."
                exit 1
            fi
        fi
    done
}

function coloredEcho() {
    local exp="$1";
    local color="$2";
    local arrow="$3";
    if ! [[ $color =~ '^[0-9]$' ]] ; then
       case $(echo $color | tr '[:upper:]' '[:lower:]') in
        black) color=0 ;;
        red) color=1 ;;
        green) color=2 ;;
        yellow) color=3 ;;
        blue) color=4 ;;
        magenta) color=5 ;;
        cyan) color=6 ;;
        white|*) color=7 ;; # white or invalid color
       esac
    fi
    tput setaf "$color";
    echo "$arrow $exp";
    tput sgr0;
}

function info() {
    coloredEcho "$1" blue "========>"
}

function substep() {
    coloredEcho "$1" magenta "===="
}

function success() {
    coloredEcho "$1" green "========>"
}

function error() {
    coloredEcho "$1" red "========>"
}

main "$@"
