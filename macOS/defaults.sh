#!/usr/bin/env bash

main() {
    pre_config_setup
    configure_plist_apps
    configure_finder
    configure_system
    configure_safari
    configure_general_ui
    configure_trackpad_keyboard_input
    configure_screen
    configure_dock_dashboard_hot_corners
    configure_mail
    configure_mac_app_store
    configure_alfred
    quit_affected_apps
    move_focus_back_to_iterm2 # to ensure the focus gets back to the terminal after the execution completes
}

function pre_config_setup() {
    quit "System Preferences"
    quit "Amphetamine"
    quit "Transmission"
    quit "The Unarchiver"
    quit "Safari"
}

function configure_plist_apps() {
    import_plist "com.if.Amphetamine" "plists/Amphetamine.plist"
    import_plist "org.m0k.transmission" "plists/Transmission.plist"
    import_plist "cx.c3.theunarchiver" "plists/The_Unarchiver.plist"
    import_plist "com.apple.Safari" "plists/com.apple.Safari.plist"

    open "Amphetamine"

    success "Importing plist files succeeded."
}

function configure_system() {
    # Disable Gatekeeper entirely to get rid of \
    # “Are you sure you want to open this application?” dialog
    sudo spctl --master-disable

    success "Configuring system succeeded."
}

function configure_dock_dashboard_hot_corners() {
    # Set the icon size of Dock items to 40 pixels
    defaults write com.apple.dock tilesize -int 40

    # Don’t animate opening applications from the Dock
    defaults write com.apple.dock launchanim -bool false

    # Disable Dashboard
    defaults write com.apple.dashboard mcx-disabled -bool true

    # Don’t show Dashboard as a Space
    defaults write com.apple.dock dashboard-in-overlay -bool true

    # Automatically hide and show the Dock
    defaults write com.apple.dock autohide -bool true

    # Remove the auto-hiding Dock delay
    defaults write com.apple.dock autohide-delay -float 0

    # Speed up Mission Control animations
    defaults write com.apple.dock expose-animation-duration -float 0.1

    add_default_apps_to_dock

    quit "Dock"

    success "Configuring dock succeeded."
}

function add_default_apps_to_dock() {
    # Wipe all (default) app icons from the Dock
    defaults write com.apple.dock persistent-apps -array

    # Add them all
    defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/System Preferences.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"
    defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Launchpad.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"
    defaults write com.apple.dock persistent-apps -array-add '{tile-data={}; tile-type="spacer-tile";}'
    defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Safari.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"
    defaults write com.apple.dock persistent-apps -array-add '{tile-data={}; tile-type="spacer-tile";}'
    defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/App Store.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"
    defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Books.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"
    defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/iTunes.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"
    defaults write com.apple.dock persistent-apps -array-add '{tile-data={}; tile-type="spacer-tile";}'
    defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Notable.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"
    defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Todoist.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"
    defaults write com.apple.dock persistent-apps -array-add '{tile-data={}; tile-type="spacer-tile";}'
    defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Sublime Text.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"
    defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Sublime Merge.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"
    defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/iTerm.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"
    defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Postgres.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"
    defaults write com.apple.dock persistent-apps -array-add '{tile-data={}; tile-type="spacer-tile";}'
    defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Transmission.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"

    success "Adding apps to the dock succeeded."
}

function configure_finder() {
    # disable window animations and Get Info animations
    defaults write com.apple.finder DisableAllAnimations -bool true

    # Display full POSIX path as Finder window title
    defaults write com.apple.finder _FXShowPosixPathInTitle -bool true

    # Keep folders on top when sorting by name
    defaults write com.apple.finder _FXSortFoldersFirst -bool true

    # When performing a search, search the current folder by default
    defaults write com.apple.finder FXDefaultSearchScope -string "SCcf"

    # Disable disk image verification
    defaults write com.apple.frameworks.diskimages \
        skip-verify -bool true
    defaults write com.apple.frameworks.diskimages \
        skip-verify-locked -bool true
    defaults write com.apple.frameworks.diskimages \
        skip-verify-remote -bool true

    # Use list view in all Finder windows by default
    # Four-letter codes for the other view modes: `icnv`, `clmv`, `Flwv`
    defaults write com.apple.finder FXPreferredViewStyle -string "Nlsv"

    # Show all file extensions
    defaults write NSGlobalDomain AppleShowAllExtensions -bool true

    success "Configuring finder succeeded."
}

function configure_safari() {
    # Disable plug-ins
    defaults write com.apple.Safari WebKitPluginsEnabled -bool false
    defaults write com.apple.Safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2PluginsEnabled -bool false

    # Make Safari’s search banners default to Contains instead of Starts With
    defaults write com.apple.Safari FindOnPageMatchesWordStartsOnly -bool false

    # Enable continuous spellchecking
    defaults write com.apple.Safari WebContinuousSpellCheckingEnabled -bool true

    # Disable auto-correct
    defaults write com.apple.Safari WebAutomaticSpellingCorrectionEnabled -bool false

    # Disable the standard delay in rendering a Web page.
    defaults write com.apple.Safari WebKitInitialTimedLayoutDelay 0.25

    # Add a context menu item for showing the Web Inspector in web views
    defaults write NSGlobalDomain WebKitDeveloperExtras -bool true

    success "Configuring Safari succeeded."
}

function configure_general_ui() {
    # Set computer name (as done via System Preferences → Sharing)
    sudo scutil --set ComputerName "beeShadow"
    sudo scutil --set HostName "beeShadow"
    sudo scutil --set LocalHostName "beeShadow"
    sudo defaults write /Library/Preferences/SystemConfiguration/com.apple.smb.server NetBIOSName -string "beeShadow"

    # Disable the sound effects on boot
    sudo nvram SystemAudioVolume=" "

    # Disable transparency in the menu bar and elsewhere
    defaults write com.apple.universalaccess reduceTransparency -bool true

    # Menu bar: show remaining battery time; hide percentage
    defaults write com.apple.menuextra.battery ShowTime -string "YES"

    # Disable opening and closing window animations
    defaults write NSGlobalDomain NSAutomaticWindowAnimationsEnabled -bool false

    # Menu bar: disable transparency
    defaults write NSGlobalDomain AppleEnableMenuBarTransparency -bool false

    # Set sidebar icon size to medium
    defaults write NSGlobalDomain NSTableViewDefaultSizeMode -int 2

    # Show scrollbars only when scrolling
    defaults write NSGlobalDomain AppleShowScrollBars -string "WhenScrolling"

    # Increase window resize speed for Cocoa applications
    defaults write NSGlobalDomain NSWindowResizeTime -float 0.001

    # Expand save panel by default
    defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode -bool true
    defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode2 -bool true

    # Reveal IP address, hostname, OS version, etc. when clicking the clock
    # in the login window
    sudo defaults write /Library/Preferences/com.apple.loginwindow AdminHostInfo HostName

    # Display the time with seconds and date
    defaults write com.apple.menuextra.clock DateFormat -string "EEE d MMM  HH:mm:ss"

    # Disable auto-correct
    defaults write NSGlobalDomain NSAutomaticSpellingCorrectionEnabled -bool false

    # Disable animations when opening a Quick Look window.
    defaults write -g QLPanelAnimationDuration -float 0

    success "Configuring general UI/UX succeeded."
}

function configure_trackpad_keyboard_input() {
    # Trackpad: enable tap to click for this user and for the login screen
    defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad Clicking -bool true
    defaults -currentHost write NSGlobalDomain com.apple.mouse.tapBehavior -int 1
    defaults write NSGlobalDomain com.apple.mouse.tapBehavior -int 1

    # Set a really fast key repeat.
    defaults write NSGlobalDomain KeyRepeat -int 2
    defaults write NSGlobalDomain InitialKeyRepeat -int 25
    defaults write NSGlobalDomain ApplePressAndHoldEnabled -bool false

    success "Configuring inputs succeeded."
}

function configure_screen() {
    # Require password immediately after sleep or screen saver begins
    defaults write com.apple.screensaver askForPassword -int 1
    defaults write com.apple.screensaver askForPasswordDelay -int 0

    success "Configuring screen succeeded."
}

function configure_mail() {
    # Display emails in threaded mode, sorted by date (oldest at the top)
    defaults write com.apple.mail DraftsViewerAttributes -dict-add "DisplayInThreadedMode" -string "yes"
    defaults write com.apple.mail DraftsViewerAttributes -dict-add "SortedDescending" -string "yes"
    defaults write com.apple.mail DraftsViewerAttributes -dict-add "SortOrder" -string "received-date"

    # Disable the animation when you sending and replying an e-mail
    defaults write com.apple.mail DisableReplyAnimations -bool true
    defaults write com.apple.mail DisableSendAnimations -bool true

    success "Configuring Mail app succeeded."
}

function configure_mac_app_store() {
    # Turn on app auto-update
    defaults write com.apple.commerce AutoUpdate -bool true

    # Enable the automatic update check
    defaults write com.apple.SoftwareUpdate AutomaticCheckEnabled -bool true

    # Check for software updates daily, not just once per week
    defaults write com.apple.SoftwareUpdate ScheduleFrequency -int 1

    # Download newly available updates in background
    defaults write com.apple.SoftwareUpdate AutomaticDownload -int 1

    success "Configuring Mac App Store succeeded."
}

function configure_alfred() {
  defaults write com.runningwithcrayons.Alfred-Preferences-3 syncfolder -string "~/Dropbox/Mac/Configurations/Alfred"
}

function quit() {
    app=$1
    killall "$app" > /dev/null 2>&1
}

function open() {
    app=$1
    osascript << EOM
tell application "$app" to activate
EOM
}

function import_plist() {
    domain=$1
    filename=$2
    defaults delete "$domain" &> /dev/null
    defaults import "$domain" "$filename"
}

function move_focus_back_to_iterm2() {
    osascript << EOM
tell application "System Events" to tell process "iTerm2"
set frontmost to true
end tell
EOM
}

function success() {
    tput setaf "2";
    echo "========> $1"
    tput sgr0;
}

function quit_affected_apps() {
    for app in "Activity Monitor" \
        "Address Book" \
        "Calendar" \
        "cfprefsd" \
        "Contacts" \
        "Dock" \
        "Finder" \
        "Google Chrome" \
        "Mail" \
        "Messages" \
        "Safari" \
        "SystemUIServer" \
        "Transmission" \
        "iCal"; do
        killall "${app}" &> /dev/null
    done
}

main "$@"
