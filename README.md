* On a fresh macOS:
	* Setup for a software development environment entirely with a one-liner 🔥
    ```
    curl --silent https://gitlab.com/chanjman/dotfiles/raw/master/setup-macos.sh | bash
    ```

	* Open a Fish shell and execute `install_oh_my_fish` function.
	* Enter license information of purchased applications.

* Execute `bootstrap` function freely which in turn executes the bootstrapping script.
